# Docker Nginx + Log Management

### Task
* Deploy a Dockerized application serving a static website via (e.g. via Nginx) that displays
a custom welcome page (but not the default page for the web server used)
* Use fluentd to ship Nginx request logs to an output of your choice (e.g. S3,
ElasticSearch)
* Provide a standalone solution including both webapp and fluentd using docker-compose
and/or a kubernetes deployment (plain manifest or helm chart)

###Notes
* Avoid running multiple services in single container
* You can use any 3rd party Docker image (you might have to explain your choice)
* Bonus: use an IAC tool of your choice to create cloud resources you may need (e.g. S3
buckets)

## Solution

* Created deploymet/services to deploy and run nginx and fluentd on kubernetes.
* Created custom home screen for nginx

To deploy and run services, create kubernetes cluster and deploy by following commands
```sh
$ kubectl create configmap fluent-config --from-file=fluentd/fluent.conf
$ kubectl apply -f deployment/elasticsearch.yaml -f deployment/kibana.yaml -f deployment/nginx.yaml -f deployment/volume.yaml
```

DEMO : https://youtu.be/Xy54O_Drtec
